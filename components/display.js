'use strict';

/**
 * Example: https://github.com/mdn/web-components-examples/tree/main
 * 
 *  #####   # 
 *      #   #
 *  #####   #####    ###
 * #    #   #    #  #
 * #    #   #    #  #
 *  ####    #####    ###
 * 
 *  ###   ###
 * #   #  #  #
 * #   #  #  #
 * #####  ###
 * #   #  #  #
 * #   #  #  #
 * #   #  ###
 */

(function() {
  class Display extends HTMLElement {
    constructor() {
      super();
      this.classList.add('nx-display');
      const shadow = this.attachShadow({ mode: 'open' });
      //<canvas id="myCanvas" width="512" height="128" style="border:1px solid grey"></canvas>
      this.$canvas = document.createElement('canvas');
      this.$canvas.width = 512;
      this.$canvas.height = 128;
      shadow.appendChild(this.$canvas);
    }

    setPixel(x,y,color="blue") {
        const scale = 4;
        this.ctx.beginPath();
        this.ctx.lineWidth = "0";
        this.ctx.strokeStyle = "red";
        this.ctx.fillStyle = color; // (x+1)* (y+1)*
        this.ctx.fillRect(x*scale, y*scale, scale, scale);  
        this.ctx.stroke();
    }

    setCol(x,y,color,chars) {
        for (var i=0; i<chars.length; i++) {
            if (chars[i] != ' ') {
                this.setPixel(x+i, y, color);
            }
        }
        return chars.length;
    }

    a(x,y,c) {
        this.setCol(x,y+0,c, "       ");
        this.setCol(x,y+1,c, " ##### ");
        this.setCol(x,y+2,c, "     # ");
        this.setCol(x,y+3,c, " ##### ");
        this.setCol(x,y+4,c, "#    # ");
        this.setCol(x,y+5,c, "#    # ");
        this.setCol(x,y+6,c, " ##### ");
        return 7;
    }

    b(x,y,c) {
      this.setCol(x,y+0,c, "#      ");
      this.setCol(x,y+1,c, "#      ");
      this.setCol(x,y+2,c, "#      ");
      this.setCol(x,y+3,c, "#####  ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#####  ");
      return 7;
    }
    
    c(x,y,c) {
        this.setCol(x,y+0,c, "       ");
        this.setCol(x,y+1,c, "       ");
        this.setCol(x,y+2,c, "       ");
        this.setCol(x,y+3,c, " ##### ");
        this.setCol(x,y+4,c, "#      ");
        this.setCol(x,y+5,c, "#      ");
        this.setCol(x,y+6,c, " ##### ");
        return 7;
    }
    d(x,y,c) {
      this.setCol(x,y+0,c, "     # ");
      this.setCol(x,y+1,c, "     # ");
      this.setCol(x,y+2,c, "     # ");
      this.setCol(x,y+3,c, " ##### ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, " ##### ");
      return 7;
    }
    e(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, "      ");
      this.setCol(x,y+2,c, " ###  ");
      this.setCol(x,y+3,c, "#   # ");
      this.setCol(x,y+4,c, "# ##  ");
      this.setCol(x,y+5,c, "#     ");
      this.setCol(x,y+6,c, " #### ");
      return 6;
    }
    f(x,y,c) {
      this.setCol(x,y+0,c, " ##   ");
      this.setCol(x,y+1,c, "#     ");
      this.setCol(x,y+2,c, "###   ");
      this.setCol(x,y+3,c, "#     ");
      this.setCol(x,y+4,c, "#     ");
      this.setCol(x,y+5,c, "#     ");
      this.setCol(x,y+6,c, "#    ");
      return 6;
    }
    g(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, " ###  ");
      this.setCol(x,y+2,c, "#   # ");
      this.setCol(x,y+3,c, "##### ");
      this.setCol(x,y+4,c, "#     ");
      this.setCol(x,y+5,c, "##### ");
      this.setCol(x,y+6,c, " #### ");
      return 6;
    }
    h(x,y,c) {
      this.setCol(x,y+0,c, "#      ");
      this.setCol(x,y+1,c, "#      ");
      this.setCol(x,y+2,c, "#      ");
      this.setCol(x,y+3,c, "#####  ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#    # ");
      return 7;
    }

    i(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, "#     ");
      this.setCol(x,y+2,c, "      ");
      this.setCol(x,y+3,c, "#     ");
      this.setCol(x,y+4,c, "#     ");
      this.setCol(x,y+5,c, "#      ");
      this.setCol(x,y+6,c, "#     ");
      return 6;
    }

    j(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, " #    ");
      this.setCol(x,y+2,c, "      ");
      this.setCol(x,y+3,c, " #    ");
      this.setCol(x,y+4,c, " #    ");
      this.setCol(x,y+5,c, " #    ");
      this.setCol(x,y+6,c, "#     ");
      return 6;
    }
    k(x,y,c) {
      this.setCol(x,y+0,c, "#     ");
      this.setCol(x,y+1,c, "#     ");
      this.setCol(x,y+2,c, "#     ");
      this.setCol(x,y+3,c, "# ##  ");
      this.setCol(x,y+4,c, "##    ");
      this.setCol(x,y+5,c, "# #   ");
      this.setCol(x,y+6,c, "#  #  ");
      return 6;
    }
    l(x,y,c) {
      this.setCol(x,y+0,c, "#     ");
      this.setCol(x,y+1,c, "#     ");
      this.setCol(x,y+2,c, "#     ");
      this.setCol(x,y+3,c, "#     ");
      this.setCol(x,y+4,c, "#     ");
      this.setCol(x,y+5,c, "#     ");
      this.setCol(x,y+6,c, " ##   ");
      return 6;
    }
    m(x,y,c) {
      this.setCol(x,y+0,c, "       ");
      this.setCol(x,y+1,c, "       ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " ####  ");
      this.setCol(x,y+4,c, "# #  # ");
      this.setCol(x,y+5,c, "# #  # ");
      this.setCol(x,y+6,c, "# #  # ");
      return 7;
    }
    m(x,y,c) {
      this.setCol(x,y+0,c, "       ");
      this.setCol(x,y+1,c, "       ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " ##    ");
      this.setCol(x,y+4,c, "#  #   ");
      this.setCol(x,y+5,c, "#  #   ");
      this.setCol(x,y+6,c, "#  #   ");
      return 7;
    }
    o(x,y,c) {
      this.setCol(x,y+0,c, "       ");
      this.setCol(x,y+1,c, "       ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " ##    ");
      this.setCol(x,y+4,c, "#  #   ");
      this.setCol(x,y+5,c, "#  #   ");
      this.setCol(x,y+6,c, " ##    ");
      return 7;
    }
    p(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, " ##   ");
      this.setCol(x,y+2,c, "#  #  ");
      this.setCol(x,y+3,c, "#  #  ");
      this.setCol(x,y+4,c, "###   ");
      this.setCol(x,y+5,c, "#     ");
      this.setCol(x,y+6,c, "#     ");
      return 6;
    }
    q(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, " ##   ");
      this.setCol(x,y+2,c, "#  #  ");
      this.setCol(x,y+3,c, "#  #  ");
      this.setCol(x,y+4,c, "####  ");
      this.setCol(x,y+5,c, "   #  ");
      this.setCol(x,y+6,c, "   #  ");
      return 6;
    }
    r(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, "      ");
      this.setCol(x,y+2,c, "      ");
      this.setCol(x,y+3,c, "# ##  ");
      this.setCol(x,y+4,c, "##    ");
      this.setCol(x,y+5,c, "#     ");
      this.setCol(x,y+6,c, "#     ");
      return 6;
    }
    s(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, "####  ");
      this.setCol(x,y+2,c, "#     ");
      this.setCol(x,y+3,c, "##### ");
      this.setCol(x,y+4,c, "    # ");
      this.setCol(x,y+5,c, "   ## ");
      this.setCol(x,y+6,c, "####   ");
      return 6;
    }
    t(x,y,c) {
      this.setCol(x,y+0,c, " #    ");
      this.setCol(x,y+1,c, " #    ");
      this.setCol(x,y+2,c, "###   ");
      this.setCol(x,y+3,c, " #    ");
      this.setCol(x,y+4,c, " #    ");
      this.setCol(x,y+5,c, " #    ");
      this.setCol(x,y+6,c, "  ##  ");
      return 6;
    }
    u(x,y,c) {
      this.setCol(x,y+0,c, "      ");
      this.setCol(x,y+1,c, "      ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " #   # ");
      this.setCol(x,y+4,c, " #   # ");
      this.setCol(x,y+5,c, " #   # ");
      this.setCol(x,y+6,c, "  ###  ");
      return 7;
    }
    v(x,y,c) {
      this.setCol(x,y+0,c, "       ");
      this.setCol(x,y+1,c, "       ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " #   # ");
      this.setCol(x,y+4,c, " ## ## ");
      this.setCol(x,y+5,c, "  # #  ");
      this.setCol(x,y+6,c, "  ###  ");
      return 7;
    }
    w(x,y,c) {
      this.setCol(x,y+0,c, "         ");
      this.setCol(x,y+1,c, "         ");
      this.setCol(x,y+2,c, "         ");
      this.setCol(x,y+3,c, " #     # ");
      this.setCol(x,y+4,c, " ##   ## ");
      this.setCol(x,y+5,c, "  # # #  ");
      this.setCol(x,y+6,c, "  ## #   ");
      return 8;
    }
    x(x,y,c) {
      this.setCol(x,y+0,c, "       ");
      this.setCol(x,y+1,c, "       ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " #   # ");
      this.setCol(x,y+4,c, " ## ## ");
      this.setCol(x,y+5,c, "  ##   ");
      this.setCol(x,y+6,c, " #  #  ");
      return 7;
    }
    x(x,y,c) {
      this.setCol(x,y+0,c, "       ");
      this.setCol(x,y+1,c, "       ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " #   # ");
      this.setCol(x,y+4,c, " ## ## ");
      this.setCol(x,y+5,c, "  ##   ");
      this.setCol(x,y+6,c, " #     ");
      return 7;
    }
    z(x,y,c) {
      this.setCol(x,y+0,c, "       ");
      this.setCol(x,y+1,c, "       ");
      this.setCol(x,y+2,c, "       ");
      this.setCol(x,y+3,c, " ##### ");
      this.setCol(x,y+4,c, "   ##  ");
      this.setCol(x,y+5,c, "  ##   ");
      this.setCol(x,y+6,c, " ##### ");
      return 7;
    }
    z0(x,y,c) {
      this.setCol(x,y+0,c, "  ####  ");
      this.setCol(x,y+1,c, " #    # ");
      this.setCol(x,y+2,c, " #    # ");
      this.setCol(x,y+3,c, " #    # ");
      this.setCol(x,y+4,c, " #    # ");
      this.setCol(x,y+5,c, " #    #");
      this.setCol(x,y+6,c, "  #### ");
      return 7;
    }
    z1(x,y,c) {
      this.setCol(x,y+0,c, "   ##  ");
      this.setCol(x,y+1,c, "  ###  ");
      this.setCol(x,y+2,c, " ## #  ");
      this.setCol(x,y+3,c, "    #  ");
      this.setCol(x,y+4,c, "    #  ");
      this.setCol(x,y+5,c, "    #  ");
      this.setCol(x,y+6,c, "  #### ");
      return 7;
    }
    z2(x,y,c) {
      this.setCol(x,y+0,c, "   ##  ");
      this.setCol(x,y+1,c, "  #  # ");
      this.setCol(x,y+2,c, " #   # ");
      this.setCol(x,y+3,c, "    #  ");
      this.setCol(x,y+4,c, "   #   ");
      this.setCol(x,y+5,c, "  #    ");
      this.setCol(x,y+6,c, " ##### ");
      return 7;
    }
    z3(x,y,c) {
      this.setCol(x,y+0,c, " ##### ");
      this.setCol(x,y+1,c, "     # ");
      this.setCol(x,y+2,c, "   ##  ");
      this.setCol(x,y+3,c, "  #    ");
      this.setCol(x,y+4,c, "   ##  ");
      this.setCol(x,y+5,c, "     # ");
      this.setCol(x,y+6,c, " ##### ");
      return 7;
    }
    z4(x,y,c) {
      this.setCol(x,y+0,c, "     # ");
      this.setCol(x,y+1,c, "    #  ");
      this.setCol(x,y+2,c, "   #   ");
      this.setCol(x,y+3,c, "  #    ");
      this.setCol(x,y+4,c, " #  #  ");
      this.setCol(x,y+5,c, " ##### ");
      this.setCol(x,y+6,c, "    #  ");
      return 7;
    }
    z5(x,y,c) {
      this.setCol(x,y+0,c, " ##### ");
      this.setCol(x,y+1,c, " #     ");
      this.setCol(x,y+2,c, " #     ");
      this.setCol(x,y+3,c, " ##### ");
      this.setCol(x,y+4,c, "     # ");
      this.setCol(x,y+5,c, " #   # ");
      this.setCol(x,y+6,c, "  ###  ");
      return 7;
    }
    z6(x,y,c) {
      this.setCol(x,y+0,c, "  #### ");
      this.setCol(x,y+1,c, " #     ");
      this.setCol(x,y+2,c, " #     ");
      this.setCol(x,y+3,c, " ####  ");
      this.setCol(x,y+4,c, " #   # ");
      this.setCol(x,y+5,c, " #   # ");
      this.setCol(x,y+6,c, "  ###  ");
      return 7;
    }
    z7(x,y,c) {
      this.setCol(x,y+0,c, " ##### ");
      this.setCol(x,y+1,c, "     # ");
      this.setCol(x,y+2,c, "    #  ");
      this.setCol(x,y+3,c, "    #   ");
      this.setCol(x,y+4,c, "   #   ");
      this.setCol(x,y+5,c, "  #    ");
      this.setCol(x,y+6,c, " #     ");
      return 7;
    }
    z8(x,y,c) {
      this.setCol(x,y+0,c, "  ###  ");
      this.setCol(x,y+1,c, " #   # ");
      this.setCol(x,y+2,c, " #   # ");
      this.setCol(x,y+3,c, "  ###  ");
      this.setCol(x,y+4,c, " #   # ");
      this.setCol(x,y+5,c, " #   # ");
      this.setCol(x,y+6,c, "  ###  ");
      return 7;
    }
    z9(x,y,c) {
      this.setCol(x,y+0,c, "  ###  ");
      this.setCol(x,y+1,c, " #   # ");
      this.setCol(x,y+2,c, " #   # ");
      this.setCol(x,y+3,c, "  #### ");
      this.setCol(x,y+4,c, "     # ");
      this.setCol(x,y+5,c, "     # ");
      this.setCol(x,y+6,c, "  ###  ");
      return 7;
    }
    uA(x,y,c) {
      this.setCol(x,y+0,c, " ####  ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, "#    # ");
      this.setCol(x,y+3,c, "###### ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#    # ");
      return 7;
    }
    uB(x,y,c) {
      this.setCol(x,y+0,c, "#####  ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, "#    # ");
      this.setCol(x,y+3,c, "#####  ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#####  ");
      return 7;
    }
    uC(x,y,c) {
      this.setCol(x,y+0,c, " #### ");
      this.setCol(x,y+1,c, "#     ");
      this.setCol(x,y+2,c, "#     ");
      this.setCol(x,y+3,c, "#     ");
      this.setCol(x,y+4,c, "#     ");
      this.setCol(x,y+5,c, "#     ");
      this.setCol(x,y+6,c, " #### ");
      return 7;
    }
    uD(x,y,c) {
      this.setCol(x,y+0,c, "#####  ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, "#    # ");
      this.setCol(x,y+3,c, "#    # ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#####  ");
      return 7;
    }
    uE(x,y,c) {
      this.setCol(x,y+0,c, "###### ");
      this.setCol(x,y+1,c, "#      ");
      this.setCol(x,y+2,c, "#      ");
      this.setCol(x,y+3,c, "#####  ");
      this.setCol(x,y+4,c, "#      ");
      this.setCol(x,y+5,c, "#      ");
      this.setCol(x,y+6,c, "###### ");
      return 7;
    }
    uF(x,y,c) {
      this.setCol(x,y+0,c, "###### ");
      this.setCol(x,y+1,c, "#      ");
      this.setCol(x,y+2,c, "#      ");
      this.setCol(x,y+3,c, "#####  ");
      this.setCol(x,y+4,c, "#      ");
      this.setCol(x,y+5,c, "#      ");
      this.setCol(x,y+6,c, "#  ");
      return 7;
    }
    uG(x,y,c) {
      this.setCol(x,y+0,c, "#####  ");
      this.setCol(x,y+1,c, "#      ");
      this.setCol(x,y+2,c, "#      ");
      this.setCol(x,y+3,c, "#  ### ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#####  ");
      return 7;
    }
    uH(x,y,c) {
      this.setCol(x,y+0,c, "#    #  ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, "#    # ");
      this.setCol(x,y+3,c, "###### ");
      this.setCol(x,y+4,c, "#    # ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#    # ");
      return 7;
    }
    uI(x,y,c) {
      this.setCol(x,y+0,c, "###  ");
      this.setCol(x,y+1,c, " #   ");
      this.setCol(x,y+2,c, " #   ");
      this.setCol(x,y+3,c, " #   ");
      this.setCol(x,y+4,c, " #   ");
      this.setCol(x,y+5,c, " #   ");
      this.setCol(x,y+6,c, "###  ");
      return 7;
    }
    uJ(x,y,c) {
      this.setCol(x,y+0,c, "#### ");
      this.setCol(x,y+1,c, "   # ");
      this.setCol(x,y+2,c, "   # ");
      this.setCol(x,y+3,c, "   # ");
      this.setCol(x,y+4,c, "   # ");
      this.setCol(x,y+5,c, " # # ");
      this.setCol(x,y+6,c, " ### ");
      return 7;
    }
    uK(x,y,c) {
      this.setCol(x,y+0,c, "#   # ");
      this.setCol(x,y+1,c, "#  #  ");
      this.setCol(x,y+2,c, "# #   ");
      this.setCol(x,y+3,c, "##    ");
      this.setCol(x,y+4,c, "# #   ");
      this.setCol(x,y+5,c, "#  #  ");
      this.setCol(x,y+6,c, "#   # ");
      return 7;
    }
    uL(x,y,c) {
      this.setCol(x,y+0,c, "#     ");
      this.setCol(x,y+1,c, "#     ");
      this.setCol(x,y+2,c, "#     ");
      this.setCol(x,y+3,c, "#     ");
      this.setCol(x,y+4,c, "#     ");
      this.setCol(x,y+5,c, "#     ");
      this.setCol(x,y+6,c, "##### ");
      return 7;
    }
    uM(x,y,c) {
      this.setCol(x,y+0,c, "#     #");
      this.setCol(x,y+1,c, "##   ##");
      this.setCol(x,y+2,c, "# # # #");
      this.setCol(x,y+3,c, "#  #  #");
      this.setCol(x,y+4,c, "#     #");
      this.setCol(x,y+5,c, "#     #");
      this.setCol(x,y+6,c, "#     #");
      return 7;
    }
    uN(x,y,c) {
      this.setCol(x,y+0,c, "#     #");
      this.setCol(x,y+1,c, "##    #");
      this.setCol(x,y+2,c, "# #   #");
      this.setCol(x,y+3,c, "#  #  #");
      this.setCol(x,y+4,c, "#   # #");
      this.setCol(x,y+5,c, "#    ##");
      this.setCol(x,y+6,c, "#     #");
      return 7;
    }
    uO(x,y,c) {
      this.setCol(x,y+0,c, " ##### ");
      this.setCol(x,y+1,c, "#     #");
      this.setCol(x,y+2,c, "#     #");
      this.setCol(x,y+3,c, "#     #");
      this.setCol(x,y+4,c, "#     #");
      this.setCol(x,y+5,c, "#     #");
      this.setCol(x,y+6,c, " ##### ");
      return 7;
    }
    uP(x,y,c) {
      this.setCol(x,y+0,c, "#####  ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, "#    # ");
      this.setCol(x,y+3,c, "#####  ");
      this.setCol(x,y+4,c, "#      ");
      this.setCol(x,y+5,c, "#      ");
      this.setCol(x,y+6,c, "#      ");
      return 7;
    }
    uQ(x,y,c) {
      this.setCol(x,y+0,c, " ##### ");
      this.setCol(x,y+1,c, "#     #");
      this.setCol(x,y+2,c, "#     #");
      this.setCol(x,y+3,c, "#     #");
      this.setCol(x,y+4,c, "#   # #");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, " #### #");
      return 7;
    }
    uR(x,y,c) {
      this.setCol(x,y+0,c, "#####  ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, "#    # ");
      this.setCol(x,y+3,c, "#####  ");
      this.setCol(x,y+4,c, "##     ");
      this.setCol(x,y+5,c, "# #    ");
      this.setCol(x,y+6,c, "#  #   ");
      return 7;
    }
    uS(x,y,c) {
      this.setCol(x,y+0,c, " ####  ");
      this.setCol(x,y+1,c, "#      ");
      this.setCol(x,y+2,c, "#      ");
      this.setCol(x,y+3,c, " ####  ");
      this.setCol(x,y+4,c, "     # ");
      this.setCol(x,y+5,c, "     # ");
      this.setCol(x,y+6,c, " ####  ");
      return 7;
    }
    uT(x,y,c) {
      this.setCol(x,y+0,c, "####### ");
      this.setCol(x,y+1,c, "   #    ");
      this.setCol(x,y+2,c, "   #    ");
      this.setCol(x,y+3,c, "   #    ");
      this.setCol(x,y+4,c, "   #    ");
      this.setCol(x,y+5,c, "   #    ");
      this.setCol(x,y+6,c, "   #    ");
      return 7;
    }
    uU(x,y,c) {
      this.setCol(x,y+0,c, "#     #");
      this.setCol(x,y+1,c, "#     #");
      this.setCol(x,y+2,c, "#     #");
      this.setCol(x,y+3,c, "#     #");
      this.setCol(x,y+4,c, "#     #");
      this.setCol(x,y+5,c, "#     #");
      this.setCol(x,y+6,c, " ##### ");
      return 7;
    }
    uV(x,y,c) {
      this.setCol(x,y+0,c, "#     #");
      this.setCol(x,y+1,c, "#     #");
      this.setCol(x,y+2,c, " #   # ");
      this.setCol(x,y+3,c, " #   # ");
      this.setCol(x,y+4,c, "  # #  ");
      this.setCol(x,y+5,c, "  # #  ");
      this.setCol(x,y+6,c, "   #   ");
      return 7;
    }
    uW(x,y,c) {
      this.setCol(x,y+0,c, "#     #");
      this.setCol(x,y+1,c, "#     #");
      this.setCol(x,y+2,c, "#     #");
      this.setCol(x,y+3,c, "#     #");
      this.setCol(x,y+4,c, " # # # ");
      this.setCol(x,y+5,c, " # # # ");
      this.setCol(x,y+6,c, "  # #  ");
      return 7;
    }
    uX(x,y,c) {
      this.setCol(x,y+0,c, "#    # ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, " #  #  ");
      this.setCol(x,y+3,c, "  ##   ");
      this.setCol(x,y+4,c, " #  #  ");
      this.setCol(x,y+5,c, "#    # ");
      this.setCol(x,y+6,c, "#    # ");
      return 7;
    }
    uY(x,y,c) {
      this.setCol(x,y+0,c, "#    # ");
      this.setCol(x,y+1,c, "#    # ");
      this.setCol(x,y+2,c, " #  #  ");
      this.setCol(x,y+3,c, "  ##   ");
      this.setCol(x,y+4,c, "  ##   ");
      this.setCol(x,y+5,c, "  ##   ");
      this.setCol(x,y+6,c, "  ##   ");
      return 7;
    }
    uZ(x,y,c) {
      this.setCol(x,y+0,c, "###### ");
      this.setCol(x,y+1,c, "     # ");
      this.setCol(x,y+2,c, "    #  ");
      this.setCol(x,y+3,c, "   #   ");
      this.setCol(x,y+4,c, " #     ");
      this.setCol(x,y+5,c, "#      ");
      this.setCol(x,y+6,c, "###### ");
      return 7;
    }
    
    print_char(x,y,color, c) {
      if ("abcdefghijklmnopqrstuvwxyz".indexOf(c) != -1) {
        return this[c](x,y,color);
      } else if ("0123456789".indexOf(c) != -1) {
        return this["z"+c](x,y,color);
      } else if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) != -1) {
        return this["u"+c](x,y,color);
      } else if (c == ' ') {
        return 7;
      } else {
        return 7;
      }
    }

    text(x,y,size, color, txt) {
        var xi = 0;
        for (var i = 0; i < txt.length; i++) {
            xi += this.print_char(x+xi, y, color, txt[i]);
        }
    }
    
    connectedCallback() { // fires after the element has been attached to the DOM
      this.ctx = this.$canvas.getContext("2d");

      this.ctx.beginPath();
      this.ctx.lineWidth = "0";
      this.ctx.strokeStyle = "red";
      this.ctx.fillStyle = "black";
      this.ctx.fillRect(0, 0, this.$canvas.width, this.$canvas.height);  
      this.ctx.stroke();
    }



  }

  customElements.define('nx-display', Display);
})();