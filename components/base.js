'use strict';

/**
 * Example: https://github.com/mdn/web-components-examples/tree/main
 * 
 * /static/components/base.js
 */

(function() {

  class Expander extends HTMLElement {
    constructor() {
      super();
    }
    set_title_content() {
      if (this.$div.style.display == 'none') {
        this.$title.innerHTML = '&blacktriangleright; ' + this.title;
      } else {
        this.$title.innerHTML = '&blacktriangledown; ' + this.title;
      }
    }
    on_title_click(e) {
      // this zeigt auf this.$title
      if (this.self.$div.style.display == 'none') {
        this.self.$div.style.display = 'block';
      } else {
        this.self.$div.style.display = 'none';
      }
      this.self.set_title_content();
    }
    connectedCallback() { 
      this.classList.add('nx-expander');
      this.$title = document.createElement('div');
      this.$title.self = this;
      this.$title.classList.add('nx-expander-title');
      this.$title.style.border = '1px #ccc solid';
      this.$title.style.backgroundColor = '#dddddd';
      this.$title.innerHTML = this.title;

      this.$div = document.createElement('div');
      this.$div.style.display = 'none';
      this.$div.style.borderLeft = '1px #ccc solid';
      this.$div.style.borderRight = '1px #ccc solid';
      this.$div.style.borderBottom = '1px #ccc solid';
      this.$div.innerHTML = this.innerHTML;
      //this.childNodes.forEach((elem)=>this.$div.append(elem))
      this.set_title_content();
      this.innerHTML = '';
      this.append(this.$title);
      this.append(this.$div);
      this.$title.addEventListener('click', this.on_title_click, false);
    }
  }
  customElements.define('nx-expander', Expander);

  class CopyableText extends HTMLElement {
    constructor() {
      super();
    }  
    copyToClipboard( str ) {
      const el = document.createElement('textarea');  // Create a <textarea> element
      el.value = str;                                 // Set its value to the string that you want copied
      el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
      el.style.position = 'absolute';                 
      el.style.left = '-9999px';                      // Move outside the screen to make it invisible
      document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
      const selected =            
        document.getSelection().rangeCount > 0        // Check if there is any content selected previously
          ? document.getSelection().getRangeAt(0)     // Store selection if found
          : false;                                    // Mark as false to know no selection existed before
      el.select();                                    // Select the <textarea> content
      document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
      document.body.removeChild(el);                  // Remove the <textarea> element
      if (selected) {                                 // If a selection existed before copying
        document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
        document.getSelection().addRange(selected);   // Restore the original selection
      }
      };
    copy_click() {
      console.log(this.txt);
      this.copyToClipboard(this.txt);
    }
    connectedCallback() { 
      this.txt = this.innerText;
      const txt = this.txt;
      this.$btn = document.createElement('div');
      this.$btn.innerHTML="Copy";
      this.style.backgroundColor = '#ccc';
      this.$btn.style.border='1px #000 solid';
      this.$btn.style.width = '100px';
      this.$btn.style.float = 'right';
      this.append(this.$btn);
      this.$btn.addEventListener('click', ()=>this.copy_click(), false);
    }
  }
  customElements.define('nx-copytext', CopyableText);
})();