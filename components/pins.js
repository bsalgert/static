'use strict';

/**
 * Filter nach Tags
 */

(function() {
  class NxPin extends HTMLElement {
    constructor() {
      super();
      this.classList.add('nx-category');
      const c = this.color(this.getColor());
      this.style = 'font-family: Arial; font-weight: bold;' + this.style;
      const text = this.innerText;
      this.innerHTML = 
        '<div style="margin-top: 2px; width: 200px;">'+
                this.pfeil(c)+
                this.lblText(c, text)+
        '</div>';
    }
    lblText(c, text) {
        const s = 'color: '+c[1]+';  border: 2px '+c[0]+' solid; border-left: 4px; background-color: '+c[2]+'; margin-left: 20px; padding-left: 10px; width: 120px; border-radius:4px;';
        return '<div style="'+s+'" class="lbl">'+text+'</div>';
    }
    pfeil(c) {
        const right = c[0];
        const left = 'transparent';
        return  `<div style="width: 0px; height: 0px; float:left;
        border-top: 11px solid transparent; 
        border-right: 11px solid `+right+`; 
        border-bottom: 11px solid transparent; 
        border-left: 11px solid `+left+`; "></div>`;
    }
    getColor() {
        const a = this.attributes['color'];
        return a===undefined ? 'black' : a.value;
    }
    color(s) {
        if (s=='red') {
            return ['#900', '#600', '#fbb'];
        } else if (s=='blue') {
            return ['#009', '#006', '#bbf'];
        }else if (s=='green') {
            return ['#090', '#030', '#bfb'];
        }else {
            return ['#666', '#222', '#eee'];
        }
    }
    
    connectedCallback() { // fires after the element has been attached to the DOM
      //const addElementButton = this.shadowRoot.querySelector('.add_button');
      //addElementButton.addEventListener('click', this.addItem, false);
    }

  }

  customElements.define('nx-pin', NxPin);
})();