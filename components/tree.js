'use strict';

/**
 * https://dumptyd.github.io/treeflex/
 * 
 * <nx-tree>
 */

(function() {
  class Tree extends HTMLElement {
    constructor() {
      super();
      this.classList.add('nx-category');
      const shadow = this.attachShadow({ mode: 'open' });
      const style = document.querySelector('#tree_style');
      const link = document.createElement('link');
      link.href = "/static/css/treeflex.css";
      link.rel = "stylesheet";
      link.id = 'tree_style';
      shadow.appendChild(link);
      var nodes = this.getNodes(this);
      const $div = document.createElement('div');
      $div.classList.add('tf-tree');
      var s = '';
      if (nodes.length>0) {
        s = this.transformNode(nodes[0]);
      }
      $div.innerHTML = '<ul>'+s+'</ul>';

      // binding methods
      //this.addItem = this.addItem.bind(this);

      shadow.appendChild($div);
    }

    getNodes(node) {
        var res = [];
        [...node.childNodes].forEach(function(elem) {
            if (elem.tagName == "NODE") {
                res.push(elem);
            }
          });
        return res;
    }

    transformNode(node) {
        console.log(node);
        var res = '<li>';
        res += '<span class="tf-nc">'+ node.attributes['title'].value+'</span>';
        const childs = this.getNodes(node);
        if (childs.length > 0) {
            res += '<ul>';
            childs.forEach(c=> {
                res += this.transformNode(c);
            });
            res += '</ul>';
        }
        res += '</li>';
        return res;
    }
    
    connectedCallback() { // fires after the element has been attached to the DOM
      //const addElementButton = this.shadowRoot.querySelector('.add_button');
      //addElementButton.addEventListener('click', this.addItem, false);
    }

  }

  customElements.define('nx-tree', Tree);
})();