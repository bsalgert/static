'use strict';

/**
 * Example: https://github.com/mdn/web-components-examples/tree/main
 */

(function() {
  class KvForm extends HTMLElement {
    constructor() {
      super();
      this.classList.add('kv-form');
      const shadow = this.attachShadow({ mode: 'open' });
      const $div = document.createElement('div');
      $div.classList.add('kv-form');
      $div.innerHTML = `
        <input class="add_button" type="button" value="+" />
        <div class="items">
          Items:
        </div>
      `;

      // binding methods
      this.addItem = this.addItem.bind(this);

      shadow.appendChild($div);
    }

    addItem(e) {
      const $items = this.shadowRoot.querySelector('.items');
      const $div = document.createElement('div');
      $div.classList.add('kv-item');
      $div.innerHTML = `
        <input type="text" placeholder="Key" class="item-key" />
        <input type="text" placeholder="Value"  class="item-value" />
      `; // TODO remove Button
      $items.appendChild($div);    
    }
    
    connectedCallback() { // fires after the element has been attached to the DOM
      const addElementButton = this.shadowRoot.querySelector('.add_button');
      addElementButton.addEventListener('click', this.addItem, false);
    }

    get items() {
      const items = {};
      [...this.shadowRoot.querySelectorAll('.kv-item')].forEach(elem => {
        const k = elem.querySelector('.item-key').value;
        const v = elem.querySelector('.item-value').value;
        items[k] = v;
      });
      return items;
    }

  }

  customElements.define('kv-form', KvForm);
})();