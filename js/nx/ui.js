

const IMG_UP = '<img src="/app/img/ico-up.png" />';
const IMG_SOURCE = '<img src="/app/img/ico-source.png" />';


function is_darkmode() {
    return document.body.classList.contains('dark');
}

function toast(message) {
    const elem = document.createElement('div');
    elem.innerHTML = message;
    elem.style['border'] = '1px #ccc solid';
    elem.style['background-color'] = '#333';
    elem.style['color'] = '#ddd';
    elem.style['padding'] = '5px';
    elem.style['top'] = '5px';
    elem.style['right'] = '5px';
    elem.style['position'] = 'absolute';
    document.body.append(elem);
    setTimeout(function() {
        document.body.removeChild(elem);
    }, 5000);
}


class UiPage {
    
    getTitle() {
        return "UiPage";
    }
    getHtmlContent() {
        return '<div>UiPage Content</div>';
    }
    /**
     * @returns {Array}
     */
    getOperations() {
        return [];
    }
    
}

/**
 * @see ui.css
 */
class Ui {
    constructor() {
        this.current = null;
        this.nav = document.querySelector('#list');
        this.content = document.querySelector('#content');
        this.h_content = document.querySelector('#main h2');
        this.content_ops = document.querySelector('#content_ops');
    }
    clearNav() {
        this.nav.innerHTML = '';
    }
    addNavItem(title, onClick) {
        var $a = document.createElement('a');
        $a.innerHTML = title;
        $a.onclick = onClick;
        this.nav.append($a);
    }
    addContentAction(title, onClick) {
        var $a = document.createElement('a');
        $a.innerHTML = title;
        $a.onclick = onClick;
        this.content_ops.append($a);
    }
    showPage(title, content) {
        this.h_content.innerHTML = title;
        this.content_ops.innerHTML = '';
        if (typeof content === 'string' || content instanceof String) {
            this.content.innerHTML = content;
        } else if (content instanceof UiPage) {
            this._showUiPage(content);
        } else {
            this.content.innerHTML = "Error: Unknown Content";
        }
    }
    /**
     * 
     * @param {UiPage} content
     */
    showUiPage(content) {
        this.current = content;
        this.content.innerHTML = content.getHtmlContent();
        this.h_content.innerHTML = content.getTitle();
        this.content_ops.innerHTML = '';
        content.getOperations().forEach(elem=>{
            this.addContentAction(elem.title, elem.action);
        });
    }
    appendContent(content) {
        var $div = document.createElement('div');
        $div.innerHTML = content;
        this.content.append($div);
    }
    enableSearch() {
        var $q = document.querySelector('#q');
        if ($q !== null) {
            $q.addEventListener("keypress", (event) => {
                var t = $q.value;
                //if (event.charCode == 13) {
                    this.nav.childNodes.forEach(elem=>{
                        elem.hidden = !elem.innerHTML.includes(t);
                    });
                //}
            });
        }

    }
}