

/**
 * @media (prefers-color-scheme:dark){
    body::after{
        content: 'd';
        display: none;
    }
}
@media (prefers-color-scheme:light){
    body::after{
        content: 'l';
        display: none;
    }
 * @returns bool
 */
function isDarkMode() {
    return (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches);
}

document.addEventListener("DOMContentLoaded", function() { 

    if (!document.body.classList.contains('light') || !document.body.classList.contains('dark')) {
        if (isDarkMode()) {
            document.body.classList.add('dark');
        }
    }

 }, false);


function dropArea(upload_selector = "#upload", upload_url, on_success, image=false) {

    var $form = $(upload_selector);
    var droppedFiles = false;
    $form.on("drag dragstart dragend dragover dragenter dragleave drop", function(e) {
          e.preventDefault(); e.stopPropagation();
    }).on("dragover dragenter", function() {
        $form.addClass("is-dragover");
    }).on("dragleave dragend drop", function() {
        $form.removeClass("is-dragover");
    }).on("drop", function(e) {
        console.log("[Upload] Drop");
        droppedFiles = e.originalEvent.dataTransfer.files;
        $form.addClass("is-uploading");
        var ajaxData = new FormData();//$form.get(0)
        if (droppedFiles) {
            if (image) {
                document.querySelector(upload_selector+'_image').innerHTML = '';
            }
            $.each( droppedFiles, function(i, file) {
                ajaxData.append( "file"+i, file );
                if (image)  {
                    let reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onloadend = function() {
                        let img = document.createElement('img')
                        img.src = reader.result
                        img.style = 'max-height: 500px;';
                        document.querySelector(upload_selector+'_image').appendChild(img)
                    }
                }
            });
        }
        $.ajax({
            url: upload_url,
            type: "POST",
            data: ajaxData,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            complete: function() { $form.removeClass("is-uploading"); },
            success: function(data) {
                console.log("[Upload] Success");
                console.log(data);
                $form.addClass( data.success == true ? "is-success" : "is-error" );
                on_success(data);
            },
            error: function() {
                console.log("[Upload] Error")
            }
        });
    });

}

function enableSearch(container_selector, item_selector) {
    const $container = document.querySelector(container_selector);
    const $items = $container.querySelectorAll(item_selector);
    const $input = document.createElement('input');
    $input.addEventListener("keydown", (event) => {
        const search_text = $input.value;
        $items.forEach(element => {
            if (element.innerHTML.includes(search_text)) {
                element.style['display'] = 'block';
            } else {
                element.style['display'] = 'none';
            }
        });
    });
    $container.insertBefore($input, $container.firstChild);
}